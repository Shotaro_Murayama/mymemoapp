package com.example.syotaro_murayama.mymemoapp;

import android.provider.BaseColumns; //カラム作成のパーツが揃っているインターフェース

public final class MemoContract { // インスタンス化されることのないクラス：final

    public MemoContract() {}

    // implements：インターフェース（戻り値・メソッド名・引数のみ → 実際の処理を持たない）の使用
    public static abstract class Memos implements BaseColumns {
        public static final String TABLE_NAME = "memos";     //DB名前
        public static final String COL_TITLE = "title";      //カラム①
        public static final String COL_NAME = "body";        //カラム②
        public static final String COL_CREATE = "created";  //カラム③
        public static final String COL_UPDATED = "updated"; //カラム④
    }

}
