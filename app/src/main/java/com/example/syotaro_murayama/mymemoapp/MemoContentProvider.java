package com.example.syotaro_murayama.mymemoapp;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import java.util.logging.LoggingPermission;

public class MemoContentProvider extends ContentProvider {

    public static final String AUTHORITY =
            "com.example.syotaro_murayama.mymemoapp.MemoContentProvider";

    //CONTENT_URIを作成
    public static final Uri CONTENT_URI =
            Uri.parse("content://" + AUTHORITY + "/" + MemoContract.Memos.TABLE_NAME); //Uri型に変換

    //Urimacherを作成
    private static final int MEMOS = 1;
    private static final int MEMO_ITEM = 2;
    private static final UriMatcher uriMatcher;
    //マッチしなかった時エラーを定義
    //クラスが呼ばれたときに1回だけ実行される静的初期化ブロックを設定
    static {
        //インスタンス化
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        //uri判定 → テーブル全体のときは、『MEMOS : 1』を返却
        uriMatcher.addURI(AUTHORITY, MemoContract.Memos.TABLE_NAME, MEMOS);
        //uri判定 → カラムのみの場合は「#」記号を返すため、『MEMO_ITEM：2』を返却
        uriMatcher.addURI(AUTHORITY, MemoContract.Memos.TABLE_NAME+"/#", MEMO_ITEM);

    }

    private MemoOpenHelper memoOpenHelper;

    public MemoContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // TODO: Implement this to handle requests to insert a new row.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public boolean onCreate() { //onCreate()：アプリ起動時に呼び出される
        //memoOpenHelper：上記で定義した変数（MemoOpenHelperクラスから作成したインスタンスを格納）
        memoOpenHelper = new MemoOpenHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(
            Uri uri,
            String[] projection,
            String selection,
            String[] selectionArgs,
            String sortOrder
    ) {
        //UriMatcher（このクラス内で定義）を使用 → 渡ってきたuriを判定
        switch(uriMatcher.match(uri)) {
            case MEMOS:
            case MEMO_ITEM:
                break;
            default:
                throw new IllegalArgumentException("Invalid URI:" + uri);
        }

        //※注意 ) ContentProvider中ではDBもCursorも「close()」する必要はない
        SQLiteDatabase db = memoOpenHelper.getReadableDatabase();
        Cursor c = db.query(
                MemoContract.Memos.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
        return c;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
