package com.example.syotaro_murayama.mymemoapp;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.lang.invoke.LambdaConversionException;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    //【SimpleCursorAdapter】 : ListViewにデータを渡すために用いられるアダプターの1つ
    //CursorLoaderを使用するためアダプターを設置
    //直接DBのデータをListView渡すことが可能
    /* → 通常SQLiteDatabaseなどのDBから特定のデータを取得してListViewに表示する際、
          データを取得してから渡す2アクション挟むことになるが直接取得が可能 */
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] from = {
                MemoContract.Memos.COL_TITLE,
                MemoContract.Memos.COL_UPDATED
        };

        int[] to = {
                android.R.id.text1,
                android.R.id.text2
        };

        adapter = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_2,//行レイアウトの指定 → 「simple_list_item_2.xml」を指定(TextView1, TextView2が存在)
                //①TextView1 : タイトルを表示させる
                //②TextView1 : アップデートを表示させる
                null,
                from,
                to,
                0
        );

        //ListViewを取得してadapterをくっつける
        ListView myListView = (ListView) findViewById(R.id.myListView);
        myListView.setAdapter(adapter);

        //★★問題のポイント
        //getLoaderManager().initLoader(0,null, this);
        getSupportLoaderManager().initLoader(0,null, this);
    }



    //[↓Loaderの設定]==============================================================================



    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
        //projection : CursorLoaderを作成するための引数で使用
        String[] projection = {
                MemoContract.Memos._ID,
                MemoContract.Memos.COL_TITLE,
                MemoContract.Memos.COL_UPDATED
        };
        return new CursorLoader(
                this,
                MemoContentProvider.CONTENT_URI,
                projection,
                null,
                null,
                MemoContract.Memos.COL_UPDATED + "DESC"
        );
    }

    //「onLoadFinished」: ContentProviderからデータが返ってきたときに実行される処理
    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        //アダプターを更新
        adapter.swapCursor(cursor);
    }

    //「onLoaderReset」: Loaderがリセットされた際に実行される処理
    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        adapter.swapCursor(null);

    }
}
