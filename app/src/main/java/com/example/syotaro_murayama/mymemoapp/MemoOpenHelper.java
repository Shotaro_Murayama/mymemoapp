package com.example.syotaro_murayama.mymemoapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

//SQLiteOpenHelperクラス：MemoContentProvider.java中の「SQLiteDatabaseクラス」で使用するため継承
public class MemoOpenHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "myapp.db"; //DB名
    public static final int DB_VERSION = 1;           //DBのバージョン

    //SQL文の定義・作成(CREATE_TABLE)
    public static final String CREATE_TABLE =
            "create table memos(" +
                    "_id integer primary key autoincrement," +      //id
                    "title text," +
                    "body text," +
                    "created datetime default current_timestamp," + //datetime default current_timestamp → 未入力の場合現在日時をセット
                    "updated datetime default current_timestamp)";  //datetime default current_timestamp → 未入力の場合現在日時をセット

    //SQL分の定義・作成(INIT_TABLE)
    public static final String INIT_TABLE =
            "insert into memos (title, body) values" +
                    "('t1', 'b1')," +
                    "('t2', 'b2')," +
                    "('t3', 'b3')";

    //SQL分の定義・作成(DROP_TABLE)
    public static final String DROP_TABLE =
            "drop table if exists " + MemoContract.Memos.TABLE_NAME; //MemoContract.java > MemosクラスのDB名前


    //コンストラクタの作成
    public MemoOpenHelper(Context c) {
        //親クラスのコンストラクタを呼ぶ必要がある??
        super(c, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //CREATE_TABLEというSQLを実行
        db.execSQL(CREATE_TABLE);
        db.execSQL(INIT_TABLE); //データ格納用

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }
}
